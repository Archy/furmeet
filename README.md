# FurMeet

<img src="https://framagit.org/Archy/furmeet/-/raw/main/furmeetdev/assets/images/logo_furmeet.png?ref_type=heads" alt="drawing" width="300"/>  


## Description courte du projet. 
    Cette application mobile Flutter vise à rapprocher les furry en facilitant la création et l'organisation de meetups.

## Captures d'écran

<img src="https://framagit.org/Archy/furmeet/-/raw/connexion/Capture_d_%C3%A9cran_du_2023-12-18_11-01-14.png" alt="drawing" width="200"/>  
<img src="https://framagit.org/Archy/furmeet/-/raw/connexion/Capture_d_%C3%A9cran_du_2023-12-18_11-40-23.png" alt="drawing" width="200"/>  

## Technologies Utilisées

<img src="https://upload.wikimedia.org/wikipedia/commons/1/17/Google-flutter-logo.png" alt="drawing" width="200"/>  
<img src="https://varialhosting.com/blog/wp-content/uploads/2021/11/PHP8.png" alt="drawing" width="200"/>   
<img src="https://assets.stickpng.com/images/62deb17dff3c6e4b8b5de8c9.png" alt="drawing" width="200"/>
<img src="https://cdn.icon-icons.com/icons2/2415/PNG/512/debian_original_logo_icon_146566.png" alt="drawing" width="200"/>
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/92/Android_Studio_Trademark.svg/512px-Android_Studio_Trademark.svg.png" alt="drawing" width="200"/>
<img src="https://martin-bozon.students-laplateforme.io/projets/ide/image/logovscode2.png" alt="drawing" width="200"/>   



## Fonctionnalités Clés

    Création, organisation et gestion de meetups.
    Connexion utilisateur via adresse e-mail(unique).
    Consultation des actualités de l'application et des meetups sans connexion.
    Recherche de meetups par zone géographique.

## Exigences Préalables

    Flutter SDK
    PHP 8.1
    MariaDB

## Architecture
Le projet suit une architecture MVVM (Modèle-Vue-VueModèle) pour une gestion claire du code.

## Communication avec le Serveur
Les données sont gérées sur un serveur privé Debian 12 avec MariaDB. La communication entre l'application et le serveur se fait via des API REST en PHP.

## Gestion des Utilisateurs
L'utilisateur doit posséder un compte pour créer, modifier et participer aux meetup. L'âge minimum requis est de 16 ans.

## Gestion des Meetups
Les créateurs de meetups peuvent désigner un modérateur pour gérer les participants.

## Contribution
Les contributions sont les bienvenues. N'hésitez pas à ouvrir une issue ou à proposer une pull request.
